-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.28-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para generic
CREATE DATABASE IF NOT EXISTS `generic` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `generic`;

-- Volcando estructura para tabla generic.footer
CREATE TABLE IF NOT EXISTS `footer` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(40) NOT NULL,
  `url` varchar(254) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla generic.footer: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `footer` DISABLE KEYS */;
INSERT INTO `footer` (`id`, `tipo`, `url`) VALUES
	(1, 'Copyright © LiderBook 2018', '');
/*!40000 ALTER TABLE `footer` ENABLE KEYS */;

-- Volcando estructura para tabla generic.main
CREATE TABLE IF NOT EXISTS `main` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `img` varchar(500) DEFAULT NULL,
  `nombre` varchar(254) NOT NULL,
  `aclaracion` varchar(100) DEFAULT NULL,
  `descripcion` varchar(2000) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla generic.main: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `main` DISABLE KEYS */;
INSERT INTO `main` (`id`, `img`, `nombre`, `aclaracion`, `descripcion`, `fecha`) VALUES
	(1, '200x200.png', 'X-MEN', 'DVD 1080p', NULL, '2011-07-11'),
	(2, '200x200.png', 'El señor de los anillos', NULL, NULL, '2013-07-13'),
	(3, 'Coco-2017-BluRay-BD.jpg', 'Coco', '(2017) BD25, BDRemux, BDRip 1080p', 'Descargar pelicula Coco BluRay Full HD 1080p en formatos BD25, BDRemux, BDRip con audios Espanol Latino, ', '2018-02-18');
/*!40000 ALTER TABLE `main` ENABLE KEYS */;

-- Volcando estructura para tabla generic.pruebaupdate
CREATE TABLE IF NOT EXISTS `pruebaupdate` (
  `id` int(11) DEFAULT NULL,
  `contador` int(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla generic.pruebaupdate: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `pruebaupdate` DISABLE KEYS */;
INSERT INTO `pruebaupdate` (`id`, `contador`) VALUES
	(1, 3),
	(2, 5),
	(3, 9);
/*!40000 ALTER TABLE `pruebaupdate` ENABLE KEYS */;

-- Volcando estructura para tabla generic.top_menu
CREATE TABLE IF NOT EXISTS `top_menu` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(40) NOT NULL,
  `url` varchar(254) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla generic.top_menu: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `top_menu` DISABLE KEYS */;
INSERT INTO `top_menu` (`id`, `tipo`, `url`) VALUES
	(1, 'Peliculas', '#!/peliculas'),
	(2, 'Juegos', '#!/juegos'),
	(3, 'Musica', '#!/musica');
/*!40000 ALTER TABLE `top_menu` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
