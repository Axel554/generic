<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';
require 'config.php';

$app = new Slim\App();



// ---------------- Welcole Message ----------------
$app->get('/', function ($request, $response, $args) {
	$response->write("Ruta no encontrada");
	return $response;
});


// -----------------------------------------
// ---------------- GET ALL ----------------
// -----------------------------------------

// ---------------- getAll TopMenu ----------------
$app->get('/top-menu', function ($request, $response, $args) {
	try {
		$db = getDB();
		$sth = $db->prepare("SELECT * FROM top_menu");
		$sth->execute();
		$res = $sth->fetchAll(PDO::FETCH_ASSOC);
		if ($res) {
			$response = $response->withJson($res);
			$db = null;
		}
	} 
	catch(PDOException $e) {
		$response->write('{"error":{"texto":'.$e->getMessage().'}}');
	}
	return $response;
});

// ---------------- getAll Main ----------------
$app->get('/main', function ($request, $response, $args) {
	try {
		$db = getDB();
		$sth = $db->prepare("SELECT * FROM main");
		$sth->execute();
		$res = $sth->fetchAll(PDO::FETCH_ASSOC);
		if ($res) {
			$response = $response->withJson($res);
			$db = null;
		}
	} 
	catch(PDOException $e) {
		$response->write('{"error":{"texto":'.$e->getMessage().'}}');
	}
	return $response;
});

// ---------------- getAll Footer ----------------
$app->get('/footer', function ($request, $response, $args) {
	try {
		$db = getDB();
		$sth = $db->prepare("SELECT * FROM footer");
		$sth->execute();
		$res = $sth->fetchAll(PDO::FETCH_ASSOC);
		if ($res) {
			$response = $response->withJson($res);
			$db = null;
		}
	} 
	catch(PDOException $e) {
		$response->write('{"error":{"texto":'.$e->getMessage().'}}');
	}
	return $response;
});

// ---------------------------------------------
// ---------------- FIN GET ALL ----------------
// ---------------------------------------------


// ---------------- getById ----------------
$app->get('/one/{id}', function ($request, $response, $args) {
	try {
		$db = getDB();
		$sth = $db->prepare("SELECT * FROM main WHERE id = :id");
		$sth->bindParam(":id", $args["id"], PDO::PARAM_INT);
		$sth->execute();
		$res = $sth->fetchAll(PDO::FETCH_ASSOC);
		if ($res) {
			$response = $response->withJson($res);
			$db = null;
		}
	} 
	catch(PDOException $e) {
		$response->write('{"error":{"texto":'.$e->getMessage().'}}');
	}
	return $response;
});


// ---------------- update ----------------
$app->put('/update', function ($request, $response) {
	try {
		$data = $request->getParams();
		$db = getDB();
		$sth = $db->prepare("UPDATE pruebaupdate SET contador=? WHERE id=?");
		$sth->execute(array($data["contador"], $data["id"]));
		$response->write('{"error":"ok"}');
	} 
	catch(PDOException $e) {
		$response->write('{"error":{"texto":'.$e->getMessage().'}}');
	}
	return $response;
});


// ---------------- PHPMailer ----------------
$app->post('/email', function ($request, $response, $args) {

	$mediaParams = $request->getParsedBody();

	require 'constants.php';
	require 'vendor/autoload.php';

	$mail = new PHPMailer( true );

    try {
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        
        $mail->Username = 'didiergulden@gmail.com';
        $mail->Password = EMAIL_PASSWORD;

        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;

        $mail->setFrom( 'didiergulden@gmail.com' );
        $mail->addAddress( 'didiergulden@gmail.com' );

        $mail->Subject = 'Ruta Email';
        $mail->Body = $mediaParams['message'];

		$mail->send();
		
    } catch (phpmailerException $e) {
		return "false";
        // echo $e->errorMessage(); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
		return "false";
        // echo $e->getMessage(); //Boring error messages from anything else!
	}
	
	return "true";
});


$app->run();

?>