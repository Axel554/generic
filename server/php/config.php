<?php

	function getDB(){
		$dbhost = "localhost";
		$dbname = "generic";
		$dbuser = "root";
		$dbpass = "";

		$mysql_conn_string = "mysql:host=$dbhost;dbname=$dbname";
		$dbConnection = new PDO($mysql_conn_string, $dbuser, $dbpass);
		$dbConnection->exec("SET NAMES 'utf8';");
		$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $dbConnection;
	}

?>