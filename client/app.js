var app = angular.module('liber', ['ngRoute']);

app.config(['$routeProvider', function ($routeProvider) {
	
	$routeProvider
		.when('/', {
			templateUrl : 'views/inicio.html',
			controller : 'inicio'
		})
		.otherwise({
			redirectTo : '/404'
		});

}]);

app.constant ('global', {
	
})