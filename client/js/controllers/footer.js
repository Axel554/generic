app.controller('footer', [ '$scope', 'initialData',
					function( $scope, initialData){

	initialData.footer()
		.then(function(res) {
			$scope.topMenu = res.data;
		})
		.catch(function (err) {
			console.error(err);                
		});
		
}]);