app.controller('inicio', [ '$scope', 'emailService', 'initialData',
    function( $scope, emailService, initialData){


	initialData.main()
		.then(function(res) {
			$scope.main = res.data;
		})
		.catch(function (err) {
			console.error(err);                
		});



	function sendEmail(message) {
        emailService.send(message)
            .then(function(res) {
                console.log(res.data);
            })
            .catch(function (err) {
                console.log(err);                
            });
	};
	

	// sendEmail({
	// 	message:"A verrrr"
	// });
	
}]);