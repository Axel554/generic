app.controller('peliculas', function ($scope, $rootScope, $http, global) {
	$scope.queryMain = function () {
		$http({
			method: 'GET',
			url :'../server/php/main.php'
		})
		.then(function (main) {
			$scope.main = main.data;
		}, function () {
			console.info('La petición falló');
		});
	};
	$scope.queryMain();
	
});