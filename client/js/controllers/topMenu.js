app.controller('topMenu', [ '$scope', 'initialData',
					function( $scope, initialData){

	initialData.topMenu()
		.then(function(res) {
			$scope.topMenu = res.data;			
		})
		.catch(function (err) {
			console.error(err);                
		});

}]);