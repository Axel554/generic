app.factory('initialData', ['$http', function($http) {

    var initialData = {};

    initialData.topMenu = function() {
        return $http.get('../server/php/top-menu');     
    };

    initialData.main = function() {
        return $http.get('../server/php/main');     
    };

    initialData.footer = function() {
        return $http.get('../server/php/footer');     
    };


    return initialData;
}]);