app.factory('emailService', ['$http', function($http) {

    var emailService = {};

    emailService.send = function(message) {
        return $http.post('../server/php/email', message);     
    };


    return emailService;
}]);